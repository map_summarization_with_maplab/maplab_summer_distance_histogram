#include "rovioli/localizer.h"

#include <aslam/common/occupancy-grid.h>
#include <gflags/gflags.h>
#include <localization-summary-map/localization-summary-map.h>
#include <loop-closure-handler/loop-detector-node.h>
#include <vio-common/pose-lookup-buffer.h>
#include <vio-common/vio-types.h>
#include <maplab-common/file-logger.h>
#include <mutex>

#include "rovioli/localizer-helpers.h"

DEFINE_uint64(
    rovioli_max_num_localization_constraints, 25u,
    "Max. number of localization constraints to process per camera. "
    "No prunning when 0.");

DEFINE_string(
  online_localized_vertices_csv_export_path, "",
   "Path to save localized vertices only on the map in CSV format");

namespace rovioli {
Localizer::Localizer(
    const summary_map::LocalizationSummaryMap& localization_summary_map,
    const bool visualize_localization)
    : localization_summary_map_(localization_summary_map),
      map_cached_lookup_(localization_summary_map) {
  current_localization_mode_ = Localizer::LocalizationMode::kGlobal;

  global_loop_detector_.reset(new loop_detector_node::LoopDetectorNode);

  CHECK(global_loop_detector_ != nullptr);
  if (visualize_localization) {
    global_loop_detector_->instantiateVisualizer();
  }

  LOG(INFO) << "Building localization database...";
  global_loop_detector_->addLocalizationSummaryMapToDatabase(
      localization_summary_map_);
  LOG(INFO) << "Done.";
}

Localizer::LocalizationMode Localizer::getCurrentLocalizationMode() const {
  return current_localization_mode_;
}

bool Localizer::localizeNFrame(
    const aslam::VisualNFrame::ConstPtr& nframe,
    vio::LocalizationResult* localization_result) const {
  CHECK(nframe);
  CHECK_NOTNULL(localization_result);

  bool result = false;
  switch (current_localization_mode_) {
    case Localizer::LocalizationMode::kGlobal:
      result = localizeNFrameGlobal(nframe, localization_result);
      break;
    case Localizer::LocalizationMode::kMapTracking:
      result = localizeNFrameMapTracking(nframe, localization_result);
      break;
    default:
      LOG(FATAL) << "Unknown localization mode.";
      break;
  }

  localization_result->summary_map_id = localization_summary_map_.id();
  localization_result->timestamp_ns = nframe->getMinTimestampNanoseconds();
  localization_result->nframe_id = nframe->getId();
  localization_result->localization_type = current_localization_mode_;
  return result;
}

bool Localizer::localizeNFrameGlobal(
    const aslam::VisualNFrame::ConstPtr& nframe,
    vio::LocalizationResult* localization_result) const {
  CHECK_NOTNULL(localization_result);

  constexpr bool kSkipUntrackedKeypoints = false;
  unsigned int num_lc_matches;
  vi_map::VertexKeyPointToStructureMatchList inlier_structure_matches;
  const bool success = global_loop_detector_->findNFrameInSummaryMapDatabase(
      *nframe, kSkipUntrackedKeypoints, localization_summary_map_,
      &localization_result->T_G_I_lc_pnp, &num_lc_matches,
      &inlier_structure_matches);
  
  if(success){
    writelocalizedvertex(*nframe, &localization_result->T_G_I_lc_pnp, success);
  }else{
    aslam::Transformation T_G_I = aslam::Transformation(
                          aslam::Quaternion(0.0, 1.0, 0.0, 0.0),
                          aslam::Position3D(
                          0., 0.,0.));
    writelocalizedvertex(*nframe, &T_G_I, success);

  }

  if (!success || inlier_structure_matches.empty()) {
    return false;
  }

  // Optionally sub-select the localizations by ensuring a good coverage over
  // the image. In case of conflicts the largest disparity angle between all
  // rays of all observations of the landmark will be used as a score.
  if (FLAGS_rovioli_max_num_localization_constraints > 0) {
    subselectStructureMatches(
        localization_summary_map_, map_cached_lookup_, *nframe,
        FLAGS_rovioli_max_num_localization_constraints,
        &inlier_structure_matches);
  }
  convertVertexKeyPointToStructureMatchListToLocalizationResult(
      localization_summary_map_, *nframe, inlier_structure_matches,
      localization_result);

  return true;
}

bool Localizer::localizeNFrameMapTracking(
    const aslam::VisualNFrame::ConstPtr& /*nframe*/,
    vio::LocalizationResult* /*localization_result*/) const {
  LOG(FATAL) << "Not implemented yet.";
  return false;
}


void Localizer::writelocalizedvertex(
  const aslam::VisualNFrame& n_frame, 
  const pose::Transformation* pnp_T_G_I, 
  const bool ransac_ok) const {


  
  static std::mutex mtx;
  static constexpr char kDelimiter[] = ", ";
  static const std::string path_vertices =
      common::concatenateFolderAndFileName(FLAGS_online_localized_vertices_csv_export_path, "vertices.csv");
  static std::unique_ptr<common::FileLogger> logger_vertices;
  static unsigned int vertex_count = 0;

  mtx.lock();

  if(vertex_count == 0){
    CHECK(common::createPath(FLAGS_online_localized_vertices_csv_export_path));
    logger_vertices.reset(new common::FileLogger(path_vertices));
    logger_vertices->writeDataWithDelimiterAndNewLine(
        kDelimiter, "vertex index", "timestamp [ns]", "position x [m]",
        "position y [m]", "position z [m]", "quaternion x", "quaternion y",
        "quaternion z", "quaternion w", "velocity x [m/s]", "velocity y [m/s]",
        "velocity z [m/s]", "acc bias x [m/s^2]", "acc bias y [m/s^2]",
        "acc bias z [m/s^2]", "gyro bias x [rad/s]", "gyro bias y [rad/s]",
        "gyro bias z [rad/s]","global localizatoin success [bool]");
  }

  // Write vertex data itself.
  const aslam::Transformation T_G_I = *pnp_T_G_I;
  const Eigen::Vector3d v_M(0.,0.,0.);
  const Eigen::Vector3d acc_bias(0.,0.,0.);
  const Eigen::Vector3d gyro_bias(0.,0.,0.);

  if (logger_vertices != nullptr) {
    logger_vertices->writeDataWithDelimiterAndNewLine(
      kDelimiter, vertex_count, n_frame.getMinTimestampNanoseconds(),
      T_G_I.getPosition(), T_G_I.getEigenQuaternion(), v_M, acc_bias,
      gyro_bias, ransac_ok);
  }

  vertex_count++;  
  
  mtx.unlock();
  

}

}  // namespace rovioli
