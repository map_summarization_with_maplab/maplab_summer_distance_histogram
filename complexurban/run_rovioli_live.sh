#!/usr/bin/env bash
source ~/maplab_summer_ws/devel/setup.bash

NCAMERA="/home/giho/maplab_summer_ws/src/maplab_summer/complexurban/ncamera.yaml" 
IMU="/home/giho/maplab_summer_ws/src/maplab_summer/complexurban/imu.yaml"
WHEELS="/home/giho/maplab_summer_ws/src/maplab_summer/complexurban/wheel.yaml"
MAP_LOC=""
MAP_OUTPUT=$1
REST=$@

rosrun rovioli rovioli \
  --alsologtostderr=1 \
  --v=1 \
  --ncamera_calibration=$NCAMERA  \
  --imu_parameters_maplab=$IMU \
  --wheel_parameters=$WHEELS \
  --publish_debug_markers \
  --datasource_type="rostopic" \
  --map_builder_save_image_as_resources=false \
  --optimize_map_to_localization_map=false \
  --save_map_folder=$MAP_OUTPUT \
  --overwrite_existing_map=true \
  --use_wheel_odometry=true \
  --vio_localization_map_folder=$MAP_LOC \
  $REST
