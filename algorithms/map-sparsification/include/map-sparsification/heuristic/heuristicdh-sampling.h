#ifndef MAP_SPARSIFICATION_HEURISTIC_HEURISTICDH_SAMPLING_H_
#define MAP_SPARSIFICATION_HEURISTIC_HEURISTICDH_SAMPLING_H_

#include <functional>
#include <string>
#include <utility>
#include <vector>

#include <maplab-common/macros.h>
#include <vi-map/unique-id.h>
#include <vi-map/vi-map.h>

#include "map-sparsification/heuristic/cost-functions/sampling-cost.h"
#include "map-sparsification/heuristic/scoring/scoring-function.h"
#include "map-sparsification/sampler-base.h"

namespace map_sparsification {
namespace sampling {

    // land_params:
    // observation counts on a mission, close count in a chunk, far count in a chunk 



class LandmarkSamplingDHFunctions : public SamplerBase {
 public:
  MAPLAB_POINTER_TYPEDEFS(LandmarkSamplingDHFunctions);

  virtual void sampleMapSegment(
      const vi_map::VIMap& map, unsigned int desired_num_landmarks,
      unsigned int time_limit_seconds,
      const vi_map::LandmarkIdSet& segment_store_landmark_id_set,
      const pose_graph::VertexIdList& segment_vertex_id_list,
      vi_map::LandmarkIdSet* summary_store_landmark_ids);

  virtual std::string getTypeString() const {
    return "LandmarkSamplingDHFunctions";
  }

  typedef struct Landmark_DH_params{
  unsigned int observations ;
  unsigned int close_count;
  unsigned int far_count;

  Landmark_DH_params(){
    observations = 0;
    close_count = 0;
    far_count = 0;
  }  
  Landmark_DH_params(unsigned int observations_,unsigned int close_count_,unsigned int far_count_ ){
    observations = observations_;
    close_count = close_count_;
    far_count = far_count_;
  }

      void close_countup(){
        close_count = close_count + 1;
      }

      void far_countup(){
        far_count = far_count + 1;
      }

      bool is_close(){
        if (close_count >= far_count){
          return true;
        }
        return false;

      }

      bool is_classfication_valid(){
        if( (close_count ==0) & (far_count ==0) ){
          return false;
        }
        return true;
      }


  }Landmark_DH_params;


 private:
  // distance debuging variable
  unsigned int _distance_dist[1000] = {0};
};

}  // namespace sampling
}  // namespace map_sparsification
#endif  // MAP_SPARSIFICATION_HEURISTIC_HEURISTICDH_SAMPLING_H_
