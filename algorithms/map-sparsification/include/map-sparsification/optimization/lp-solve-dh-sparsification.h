#ifndef MAP_SPARSIFICATION_OPTIMIZATION_LP_SOLVE_DH_SPARSIFICATION_H_
#define MAP_SPARSIFICATION_OPTIMIZATION_LP_SOLVE_DH_SPARSIFICATION_H_

#include <string>
#include <unordered_map>
#include <vector>

#include <Eigen/Core>
#include <vi-map/unique-id.h>
#include <vi-map/vi-map.h>

#include "map-sparsification/sampler-base.h"

namespace map_sparsification {

// Forward declaration of a wrapper that contains a pointer to a lp_solve
// lprec structure. By using it, we avoid populating all the
// global namespace definitions of lp_solve to our project.
struct LprecWrapper;

class LpSolveDHSparsification : public SamplerBase {
 public:
  typedef std::unordered_map<vi_map::LandmarkId, unsigned int>
      StoreLandmarkIdToIndexMap;
  typedef std::unordered_map<vi_map::LandmarkId, double>
      StoreLandmarkIdTodistancecostMap;

  explicit LpSolveDHSparsification(unsigned int min_keypoints_per_keyframe)
      : num_variables_(0u),
        min_keypoints_per_keyframe_(min_keypoints_per_keyframe) {}

  virtual void sampleMapSegment(
      const vi_map::VIMap& map, unsigned int desired_num_landmarks,
      unsigned int time_limit_seconds,
      const vi_map::LandmarkIdSet& segment_store_landmark_id_set,
      const pose_graph::VertexIdList& segment_vertex_id_list,
      vi_map::LandmarkIdSet* summary_store_landmark_ids);

  virtual std::string getTypeString() const {
    return "lp_solve_dh_ilp";
  }


  enum class CLASSIFIED{
      UNKNOWN, 
      CLOSE, 
      FAR
  };   



  typedef struct Landmark_DH_params{
 


    unsigned int close_count;
    unsigned int far_count;
    CLASSIFIED classification;

    Landmark_DH_params(){
      close_count = 0;
      far_count = 0;
      classification = CLASSIFIED::UNKNOWN;
    }  
    Landmark_DH_params(unsigned int close_count_,unsigned int far_count_ ){
      close_count = close_count_;
      far_count = far_count_;
      classification = CLASSIFIED::UNKNOWN;
    }

        void close_countup(){
          close_count = close_count + 1;
        }

        void far_countup(){
          far_count = far_count + 1;
        }

        bool is_close() const{
          if (close_count >= far_count){
            return true;
          }
          return false;

        }

        bool is_classfication_valid() const{
          if( (close_count ==0) & (far_count ==0) ){
            return false;
          }
          return true;
        }


        CLASSIFIED get_classification() const{
          if ( ! is_classfication_valid() ){
            return CLASSIFIED::UNKNOWN;
          }
          if ( is_close() ){
            return CLASSIFIED::CLOSE;
          }else{
            return CLASSIFIED::FAR;
          }
        }

    }Landmark_DH_params;

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

 private:
  void addKeyframeConstraint(
      unsigned int min_keypoints_per_keyframe,
      const vi_map::LandmarkIdSet& keyframe_landmarks,
      const LprecWrapper& lprec_ptr) const;
  void addTotalLandmarksConstraint(
      unsigned int desired_num_landmarks, const LprecWrapper& lprec_ptr) const;
  void addObjectiveFunction(
      const vi_map::VIMap& map, const LprecWrapper& lprec_ptr) const;
  void addDHObjectiveFunction(
      const vi_map::VIMap& map, const LprecWrapper& lprec_ptr) const;
  void setLandmarkSwitchVariablesToBinary(const LprecWrapper& lprec_ptr) const;

  void addDHObjectiveFunction(
    const vi_map::VIMap& map, const LprecWrapper& lprec_ptr, \
     const vi_map::LandmarkIdList& all_landmark_ids, const std::vector<pose_graph::VertexIdList>& segment_vertex_id_lists) const;
  void setDistancecosts(const vi_map::VIMap& map, const vi_map::LandmarkIdList& all_landmark_ids, \
    const std::vector<pose_graph::VertexIdList>& segment_vertex_id_lists, StoreLandmarkIdTodistancecostMap& landmark_ids_to_distance_costs) const;

  vi_map::VIMap map_;
  StoreLandmarkIdToIndexMap landmark_ids_to_indices_;
  unsigned int num_variables_;
  unsigned int min_keypoints_per_keyframe_;
};

}  // namespace map_sparsification
#endif  // MAP_SPARSIFICATION_OPTIMIZATION_LP_SOLVE_DH_SPARSIFICATION_H_
