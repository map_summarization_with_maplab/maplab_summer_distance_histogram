#include "map-sparsification/heuristic/heuristicdh-sampling.h"

#include <random>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <math.h>
namespace map_sparsification {
namespace sampling {



void LandmarkSamplingDHFunctions::sampleMapSegment(
    const vi_map::VIMap& map, unsigned int desired_num_landmarks,
    unsigned int /*time_limit_seconds*/,
    const vi_map::LandmarkIdSet& segment_landmark_id_set,
    const pose_graph::VertexIdList& /*segment_vertex_id_list*/,
    vi_map::LandmarkIdSet* summary_landmark_ids) {
  CHECK_NOTNULL(summary_landmark_ids)->clear();

  const size_t num_landmarks = segment_landmark_id_set.size();

  if (num_landmarks == 0u) {
    LOG(WARNING) << "No landmarks provided, bailing out early.";
    return;
  }
  
  if (num_landmarks <= desired_num_landmarks) {
    LOG(WARNING) << "Desired landmark count " << desired_num_landmarks
                 << " larger or equal to the total landmark count "
                 << num_landmarks << ", bailing out early.";
    summary_landmark_ids->insert(
        segment_landmark_id_set.begin(), segment_landmark_id_set.end());
    return;
  }

  /*
  while(first mission to last mission){
   // In a mission
   //while(first vertex to end vertex){
    while( accumated_length >= kchuncklength  ){
      Collect vertex list / accumulate change of translation and rotation
      Collect all landmarks (set) observed by a chunk of vertices 
    } 
    Calculate distance / make distance histograms sweeping a chunk
    Score observation count
    Classify landmarks
    Determine remaining ratio and store landmarks to summary landmarks list
     } // while(first vertex to end vertex)
   } // while(first mission to last mission)
  */

  // TODO
  // adjustment of greedy min K landmark selection per keyframe(vertex)
  std::cout <<"code starts landmarks spasification with distance histogram" << std::endl;
  // landmarks sparsification is conducted for each missions.


  // const variable
  const double kchuncklength = 30;

  // r_t_ratios: variable for paramerter setting
  std::vector<double> r_t_ratios;


  // parameter in reserve should depend on num_keep_landmarks and traveled distance with kchuncklength
  // here preliminary set desired_num_landmarks as a parameter of reserve
  // actually desired_num_landmarks is target num_keep_landmarks
  // here, desired_num_landmarks is number keeping landmarks per mission.
  summary_landmark_ids->reserve(desired_num_landmarks);

  

  vi_map::MissionIdList mission_ids;
  map.getAllMissionIds(&mission_ids);
  CHECK(!mission_ids.empty());


  // for(first mission to last mission)
  for (const vi_map::MissionId& mission_id : mission_ids){
    // In a mission

    const vi_map::VIMission& mission = map.getMission(mission_id);
    pose_graph::VertexId current_vertex_id = mission.getRootVertexId();
    pose_graph::VertexId next_vertex_id = current_vertex_id;


    std::unordered_map<vi_map::LandmarkId, Landmark_DH_params > observed_landmark_ids;
    pose_graph::VertexIdList a_chunk_of_vertex_ids;
    // a_chunk_of_vertex_ids.push_back(id);

    double length_a_mission;
    map.getDistanceTravelledPerMission(mission_id, &length_a_mission);

    // for paramerter setting
    r_t_ratios.clear();
    r_t_ratios.reserve(static_cast<unsigned int>(floor(kchuncklength / length_a_mission * desired_num_landmarks)));


    double translation = 0;
    double rotation = 0;
    
    // traverse mission - while(first vertex to end vertex)
    do{
      // accumulate change of translation and rotation
      const vi_map::Vertex& current_vertex = map.getVertex(current_vertex_id);
      const vi_map::Vertex& next_vertex = map.getVertex(next_vertex_id);

      CHECK_EQ(next_vertex.getMissionId(), current_vertex.getMissionId());

      // add vertex id (next_vertex_id). it will include all vertex in a chunk
      // from start vertex to end vertex.
      a_chunk_of_vertex_ids.push_back(next_vertex_id);

      // add landmarks observed by a vertex (next_vertex)
      vi_map::LandmarkIdList landmarks;
      next_vertex.getAllObservedLandmarkIds(&landmarks);
      for (const vi_map::LandmarkId& landmark_id : landmarks) {
        if (!landmark_id.isValid()) {
          continue;
        }

        // score observation vertices counts
        Landmark_DH_params land_dh_params = Landmark_DH_params( \
          map.getLandmark(landmark_id).numberOfObserverVertices(), 0, 0);

        observed_landmark_ids[landmark_id] = land_dh_params;
      }



      // accumulate translation
      translation += (next_vertex.get_p_M_I() - current_vertex.get_p_M_I()).norm();
      
      // accumulate rotation / unit: radian
      rotation += std::abs(next_vertex.get_q_M_I().angularDistance(current_vertex.get_q_M_I()));



      
      // grouping vertices on a chunk, 
      // classify landmarks, 
      // Determine remaining ratio and store landmarks to summary landmarks list. 
      if (translation >= kchuncklength){
        //std::cout << "translation: " << translation <<  "  rotation: " << rotation << "  ratio: r/t * 1000: " << rotation/translation*1000. << std::endl;
        // for parameter setting
        r_t_ratios.push_back(rotation/translation);
        //std::cout << next_vertex.get_q_M_I().angularDistance(current_vertex.get_q_M_I()) <<std::endl;

        /*
        std::cout << "In a chunk: num of [vertices: " << a_chunk_of_vertex_ids.size() <<" /" \
        " landmarks: " << observed_landmark_ids.size() << \
        " per vertex: "  << observed_landmark_ids.size() / a_chunk_of_vertex_ids.size() <<  "]" <<std::endl;
        */
        // distance debug
        std::fill(_distance_dist,&_distance_dist[999],0);

        // count close or far distance between vertices corresponding observed landmarks
        for(const pose_graph::VertexId vertex_id : a_chunk_of_vertex_ids ){
          
          const vi_map::Vertex& a_vertex = map.getVertex(vertex_id);
          vi_map::LandmarkIdList landmarks;
          a_vertex.getAllObservedLandmarkIds(&landmarks);

          for(const vi_map::LandmarkId a_landmark_id : landmarks){
            
            if (!a_landmark_id.isValid()) {
              continue;
            }
            // calculate distacne between  a vertex and landmark
            Eigen::Vector3d I_p_fi = map.getLandmark_p_I_fi(a_landmark_id, a_vertex); 
            double distance = I_p_fi.norm();           
            // distance debuging
            // std::cout << "distance: " << distance << std::endl;
            unsigned int _idx = static_cast<unsigned int>(distance/10);
            if (_idx > 999){
              _idx = 999;
            }
            _distance_dist[ _idx ]++;

            // TODO determine distance_threshold
            // make distance histogram
            double distance_threshold = 5.0;
            double distance_max_threshold = 500.;
            if(distance < distance_threshold){
              //countup close
              observed_landmark_ids[a_landmark_id].close_countup();
            }else if(distance < distance_max_threshold){
              //countup far
              observed_landmark_ids[a_landmark_id].far_countup();
            }

            
          }

        }

        // distance debuggin display
        /*
        std::cout << "" << std::endl;
        for ( unsigned int iter = 0; iter < 1000 ; iter++ ){
          if (_distance_dist[iter] != 0){
            std::cout <<"["<< iter <<"0m]:" <<  _distance_dist[iter] << " ";
          }
        }
        std::cout << "" << std::endl;
        */

        // landmarkID and sort list by observation score
        typedef std::pair<vi_map::LandmarkId, Landmark_DH_params> l_dh_vaule_type;
        typedef std::pair<vi_map::LandmarkId, unsigned int> l_score_type;
        std::vector<l_score_type> close_landmarks;
        std::vector<l_score_type> far_landmarks;

        //classify and divide lists on close and far landmarks
        unsigned int num_of_invaid_classification_landmarks = 0;
        for(const l_dh_vaule_type& observed_landmark_id : observed_landmark_ids){
          vi_map::LandmarkId landmark_id = observed_landmark_id.first;
          Landmark_DH_params landmark_param = observed_landmark_id.second;

          if (landmark_param.is_classfication_valid()){
            if(landmark_param.is_close()){
              close_landmarks.push_back(l_score_type(landmark_id,landmark_param.observations));
            }else{
              far_landmarks.push_back(l_score_type(landmark_id,landmark_param.observations));
            }
          }else{
            num_of_invaid_classification_landmarks++;
          }
        }
        //std::cout << "[num of invalid landmark in a chunk:" << num_of_invaid_classification_landmarks << "]" << std::endl;
        // sort close and far list by observation score
        std::sort(close_landmarks.begin(), close_landmarks.end(), \
         [](const l_score_type &x, const l_score_type &y){
            return x.second > y.second;
        });

        std::sort(far_landmarks.begin(), far_landmarks.end(), \
         [](const l_score_type &x, const l_score_type &y){
            return x.second > y.second;
        });

        std::cout << "num of [close landmark: " << close_landmarks.size() << "   ,far landmark: " << far_landmarks.size() << "]" << std::endl;
        //for(auto item : close_landmarks) { std::cout << item.first << "   ,  " << item.second << std::endl; }
        // Determine remaining ratio of close and far (here n/2 where n=200), then 

        // determine ratio of remaining_close and remaining_far with rotation and translation
        // determine number of keeping landmarks in a chunk depending on translation
        unsigned int num_landmarks_a_chunk = \
         static_cast<unsigned int>(floor(translation / length_a_mission * desired_num_landmarks)); 
        // std::cout << "num_landmarks_a_chunk: " << num_landmarks_a_chunk << std::endl;
        
        double k_rota_tran = 82.356*2.5; // linear value : 82.356 / exponential value
        // at least 25 % for rotation and translation same
        // double global_ratio_far = 0.25; 
        // double global_ratio_close = 0.25;

        // version 1: linear ratio
        //double remaining_far_ratio = \
         std::max( std::min( rotation / translation * k_rota_tran , 0.5), 0.) + global_ratio_far;

        //version 2: exponential ratio
        double remaining_far_ratio = \
         exp(k_rota_tran*rotation) / ( exp(k_rota_tran*rotation) + exp(translation) );

        unsigned int remaining_far = static_cast<unsigned int>( floor( num_landmarks_a_chunk * remaining_far_ratio));
        unsigned int remaining_close = num_landmarks_a_chunk - remaining_far;



        std::cout << "[r/t*k_r_t: " << rotation / translation * k_rota_tran <<  \
        ", far_ratio: " << remaining_far_ratio << \
        ", num_close: " << remaining_close << ", num_far: " << remaining_far << std::endl;
        // remove landmarks by score
        if (close_landmarks.size() > remaining_close ){
          close_landmarks.erase(close_landmarks.begin() + remaining_close, close_landmarks.end());
        }

        if (far_landmarks.size() > remaining_far ){
          far_landmarks.erase(far_landmarks.begin() + remaining_far, far_landmarks.end());
        }

        //std::cout << "Aft num of [close landmark: " << close_landmarks.size() << "   ,far landmark: " << far_landmarks.size() << "]\n" << std::endl;

        // add filtered close and far landmarks to summary_landmark_ids
        for (l_score_type& landmark_with_score : close_landmarks ){
          summary_landmark_ids->emplace(landmark_with_score.first);
        }
        for (l_score_type& landmark_with_score : far_landmarks ){
          summary_landmark_ids->emplace(landmark_with_score.first);
        }
        // clear translation / rotation / list of vertices and landmarks
        translation = 0;
        rotation = 0;
        observed_landmark_ids.clear();
        a_chunk_of_vertex_ids.clear();
        far_landmarks.clear();
        close_landmarks.clear();
      }


      current_vertex_id = next_vertex_id;
    }while (map.getNextVertex(
        current_vertex_id, map.getGraphTraversalEdgeType(mission_id),
        &next_vertex_id));  //while(first vertex to end vertex)

    // for paramerter setting


    /*
    std::sort(r_t_ratios.begin(),r_t_ratios.end());
    unsigned int for_high_5_percent = r_t_ratios.size()*95/100;
    double r_t_ratio;

    for(auto it = r_t_ratios.begin() + for_high_5_percent ;it != r_t_ratios.end(); it++){
      r_t_ratio = *it;
      std::cout << r_t_ratio << "    ";
    }
    double k_r_t = 0.5/(*(r_t_ratios.begin() + for_high_5_percent));
    std::cout << "[rotation_translation constant: " << k_r_t << std::endl;
    

    for(auto it = r_t_ratios.begin() + for_high_5_percent ;it != r_t_ratios.end(); it++){
      r_t_ratio = *it;
      std::cout << r_t_ratio*k_r_t << "    ";
    }
    */


  } // for(first mission to last mission)




  std::cout <<"number of remaning landmarks: " << summary_landmark_ids->size() << std::endl;
  std::cout <<"code ends landmarks spasification with distance histogram" << std::endl;


    

}

}  // namespace sampling
}  // namespace map_sparsification
