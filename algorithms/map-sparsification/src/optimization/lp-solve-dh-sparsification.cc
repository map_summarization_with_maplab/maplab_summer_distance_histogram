#include "map-sparsification/optimization/lp-solve-dh-sparsification.h"

#include <limits>
#include <memory>
#include <vector>

#include <glog/logging.h>
#include <lp_solve/lp_lib.h>

#include "map-sparsification/optimization/lprec-wrapper.h"

namespace map_sparsification {

void LpSolveDHSparsification::sampleMapSegment(
    const vi_map::VIMap& map, unsigned int desired_num_landmarks,
    unsigned int /*time_limit_seconds*/,
    const vi_map::LandmarkIdSet& segment_landmark_id_set,
    const pose_graph::VertexIdList& segment_vertex_id_list,
    vi_map::LandmarkIdSet* summary_landmark_ids) {
  CHECK_NOTNULL(summary_landmark_ids)->clear();


  // TODO (2) Implement score function and run
  // TODO (3) Check evaluation code
  // TODO (4) Run evaluation code

  // Check if vertices are connected by index.
  pose_graph::VertexId current_vertex_id;
  pose_graph::VertexId next_vertex_id;
  vi_map::MissionId mission_id;

  // From below code, verices is mixed abount the sequences by edge.
  /*
  unsigned int mismatch_count = 0;
  for (unsigned int_idx = 0; int_idx < segment_vertex_id_list.size()-1; int_idx++){
    current_vertex_id = segment_vertex_id_list[int_idx];
    mission_id = map.getVertex(current_vertex_id).getMissionId();
    map.getNextVertex(
        current_vertex_id, map.getGraphTraversalEdgeType(mission_id),
        &next_vertex_id);
    
    if(next_vertex_id == segment_vertex_id_list[int_idx+1] ){

    }else{
      
      auto it = find(segment_vertex_id_list.begin(),segment_vertex_id_list.end(),next_vertex_id);
      if(it != segment_vertex_id_list.end()){
        unsigned int next_vertex_index_to_graph = std::distance(segment_vertex_id_list.begin(), it);
        std::cout << "this index [" << next_vertex_index_to_graph << "] should be here: " << "[" << int_idx+1 << "]" <<std::endl;
      }else{
        std::cout << "there is no next vertex in this vertices list. end this index [" << int_idx<< "] and add another vertice list from [" << int_idx+1 << "]" << std::endl;
      }
      
      //std::cout << "mismatched vertex: [cur id: " << current_vertex_id << " next id from index: " << segment_vertex_id_list[int_idx+1] << " next id from graph: " << next_vertex_id << std::endl;
      mismatch_count++;
    }
  }
  std::cout << "[total mismatched index count: " << mismatch_count << "]" << std::endl;
  if(mismatch_count != 0){
    std::cout << "segment_vertex_id_list looks independent chunks. make connected vertice lists from pose_graph::VertexIdList& segment_vertex_id_list" << std::endl;
    // std::vector<pose_graph::VertexIdList&> segment_vertex_id_lists;
    // segment_vertex_id_lists.emplace(each connected vertice list)
  }
  */

  //find starting vertcies
  unsigned int is_before_vertex[segment_vertex_id_list.size()] = {0};
  std::vector<unsigned int> starting_vertcies_idx;
  std::vector<pose_graph::VertexId>::const_iterator it;
  
  for (unsigned int_idx = 0; int_idx < segment_vertex_id_list.size(); int_idx++){
    // get next vertex id from graph
    current_vertex_id = segment_vertex_id_list[int_idx];
    mission_id = map.getVertex(current_vertex_id).getMissionId();
    map.getNextVertex(
        current_vertex_id, map.getGraphTraversalEdgeType(mission_id),
        &next_vertex_id);

    //check if next vertex is on the list.
    it = find(segment_vertex_id_list.begin(),segment_vertex_id_list.end(),next_vertex_id);
    if(it != segment_vertex_id_list.end()){
      unsigned int next_vertex_index = std::distance(segment_vertex_id_list.begin(), it);
      is_before_vertex[next_vertex_index]++;
    }
  }

  for(unsigned int int_idx = 0; int_idx< segment_vertex_id_list.size(); int_idx++ ){
    if(is_before_vertex[int_idx] == 0){
      starting_vertcies_idx.push_back(int_idx);
      std::cout << "[starting vertex: " << int_idx << "]" << std::endl;

    }else if(is_before_vertex[int_idx] > 1){
      std::cout << "[cross vertex: " << int_idx << "]: " << is_before_vertex[int_idx] << "times" << std::endl;
    }
  }


  // connect clustered graph
  std::vector<pose_graph::VertexIdList> segment_vertex_id_lists;

  for(unsigned int start_vertex_idx : starting_vertcies_idx){
    pose_graph::VertexIdList vertex_id_list;
    vertex_id_list.push_back(segment_vertex_id_list[start_vertex_idx]);
    segment_vertex_id_lists.push_back(vertex_id_list);
  }
  std::cout << "[the number of connected vertices: " << segment_vertex_id_lists.size() << "]" << std::endl;

  unsigned int prevent_double_use[segment_vertex_id_list.size()] = {0};
  unsigned int next_vertex_index;
  for(pose_graph::VertexIdList& vertex_id_list : segment_vertex_id_lists){
    
    while(1){
      current_vertex_id = vertex_id_list.back();
      mission_id = map.getVertex(current_vertex_id).getMissionId();
      map.getNextVertex(
          current_vertex_id, map.getGraphTraversalEdgeType(mission_id),
          &next_vertex_id);
      it = find(segment_vertex_id_list.begin(),segment_vertex_id_list.end(),next_vertex_id);
      if(it != segment_vertex_id_list.end()){
        next_vertex_index = std::distance(segment_vertex_id_list.begin(), it);
        if (prevent_double_use[next_vertex_index] == 0){
          vertex_id_list.push_back(segment_vertex_id_list[next_vertex_index]);
          prevent_double_use[next_vertex_index]++;
        }else{
          // stop when a vertex try to insert lists more than 2 times.
          std::cout << "[connected vertices end condition: more than 1 check ,a double-checked vertex: " << next_vertex_index << "]" << std::endl;
          break;
        }
      }else{
        std::cout << "[connected vertices end condition: no more next vertex]" << std::endl;
        break;
      }
    }
  }
  std::cout << "[given total number of vertices: " << segment_vertex_id_list.size() << "]" << std::endl;
  for(unsigned int int_idx = 0; int_idx < segment_vertex_id_lists.size(); int_idx++){
    std::cout << "[" << int_idx + 1 << "th vertices chunk size: " <<  segment_vertex_id_lists[int_idx].size() << "]" << std::endl;
  }

  // check it is well connected
  for(pose_graph::VertexIdList& _segment_vertex_id_list : segment_vertex_id_lists){
    unsigned int num_vertex_count = 0;
    for(unsigned int int_idx = 0; int_idx < _segment_vertex_id_list.size(); int_idx++ ){
      current_vertex_id = _segment_vertex_id_list[int_idx];
      map.getNextVertex(
          current_vertex_id, map.getGraphTraversalEdgeType(mission_id),
          &next_vertex_id);
      if(current_vertex_id != _segment_vertex_id_list.back()){
        if(next_vertex_id == _segment_vertex_id_list[int_idx+1]){
          num_vertex_count++;
        }else{
          std::cout << "it isn't well connected" << std::endl;
        }
      }
    }
    std::cout << "[num of well connected vertices: " << num_vertex_count + 1 << "]" << std::endl;
  }







  // Reset class members.
  landmark_ids_to_indices_.clear();
  num_variables_ = 0u;

  // Bail out early if the desired count is smaller than the current count.
  if (segment_landmark_id_set.size() <= desired_num_landmarks) {
    LOG(WARNING) << "Nothing to summarize, bailing out early.";
    summary_landmark_ids->insert(
        segment_landmark_id_set.begin(), segment_landmark_id_set.end());
    return;
  }

  vi_map::LandmarkIdList all_landmark_ids(
      segment_landmark_id_set.begin(), segment_landmark_id_set.end());

  for (unsigned int i = 0; i < all_landmark_ids.size(); ++i) {
    // +1 to keep consistency with lp-solve variable indexing (starts with 1).
    landmark_ids_to_indices_.emplace(all_landmark_ids[i], i + 1);
  }

  // We don't know the exact constraint number right now, the problem will be
  // extended on the fly.
  const unsigned int kInitialConstraintCount = 0;
  // Adding 1 because our state consists of switch variable for each landmark
  // AND a slack variable.
  num_variables_ = all_landmark_ids.size() + 1;

  // Construct lprec object.
  LprecWrapper lprec_ptr;
  lprec_ptr.lprec_ptr = make_lp(kInitialConstraintCount, num_variables_);
  CHECK_NOTNULL(lprec_ptr.lprec_ptr);

  // Set verbose to show only critical messages.
  set_verbose(lprec_ptr.lprec_ptr, CRITICAL);

  std::string lp_name = "ILP landmark summary";
  char* c_lp_name = &lp_name[0u];
  set_lp_name(lprec_ptr.lprec_ptr, c_lp_name);

  addDHObjectiveFunction( map, lprec_ptr, all_landmark_ids, segment_vertex_id_lists);

//  addObjectiveFunction(map, lprec_ptr);
  addTotalLandmarksConstraint(desired_num_landmarks, lprec_ptr);

  for (const pose_graph::VertexId& vertex_id : segment_vertex_id_list) {
    vi_map::LandmarkIdList observer_landmarks;
    map.getVertex(vertex_id).getAllObservedLandmarkIds(&observer_landmarks);
    vi_map::LandmarkIdSet landmark_observers;
    for (const vi_map::LandmarkId& landmark_id : observer_landmarks) {
      if (landmark_id.isValid()) {
        landmark_observers.emplace(landmark_id);
      }
    }
    addKeyframeConstraint(
        min_keypoints_per_keyframe_, landmark_observers, lprec_ptr);
  }

  setLandmarkSwitchVariablesToBinary(lprec_ptr);

  // Using Basis Factorization Package (LU decomposition).
  static char kBfpConfigurationString[] = "bfp_LUSOL";
  set_BFP(lprec_ptr.lprec_ptr, kBfpConfigurationString);
  set_scaling(lprec_ptr.lprec_ptr, SCALE_CURTISREID);
  // Example presolver call from lp_solve documentation.
  set_presolve(
      lprec_ptr.lprec_ptr, PRESOLVE_ROWS | PRESOLVE_COLS | PRESOLVE_LINDEP,
      get_presolveloops(lprec_ptr.lprec_ptr));

  // It seems NODE_RANGESELECT does to trick for the optimizer to realize it
  // need to adapt the slack variable. Other flags are default ones for
  // lp_solve.
  set_bb_rule(
      lprec_ptr.lprec_ptr,
      NODE_RANGESELECT | NODE_GREEDYMODE | NODE_DYNAMICMODE | NODE_RCOSTFIXING);

  int ret = solve(lprec_ptr.lprec_ptr);
  switch (ret) {
    case NOMEMORY:
      LOG(ERROR) << "Couldn't solve ILP summarization problem,"
                 << " ran out of memory.";
      return;
      break;
    case OPTIMAL:
      LOG(INFO) << "Optimal solution found.";
      break;
    case SUBOPTIMAL:
      LOG(WARNING) << "Possibly suboptimal solution found.";
      break;
    case INFEASIBLE:
      LOG(ERROR) << "ILP summarization problem infeasible.";
      return;
      break;
    case UNBOUNDED:
      LOG(ERROR) << "ILP summarization problem unbounded.";
      return;
      break;
    case DEGENERATE:
      LOG(ERROR) << "ILP summarization problem degenerate.";
      return;
      break;
    case NUMFAILURE:
      LOG(ERROR) << "Numerical failure when solving ILP summarization problem.";
      return;
      break;
    case TIMEOUT:
      LOG(ERROR) << "ILP summarization solver timeout.";
      return;
      break;
    default:
      LOG(ERROR) << "Solver returned an error with code: " << ret;
      break;
  }

  REAL* variables_ptr;
  get_ptr_variables(lprec_ptr.lprec_ptr, &variables_ptr);
  CHECK_NOTNULL(variables_ptr);

  if (variables_ptr[num_variables_ - 1] > 0) {
    LOG(WARNING) << "Couldn't find a solution for a problem with minimum "
                 << min_keypoints_per_keyframe_
                 << " keypoints per keyframe. Slack variable had to be used, "
                 << "value: " << variables_ptr[num_variables_ - 1];
  }

  unsigned int num_landmarks_left = 0;
  for (const StoreLandmarkIdToIndexMap::value_type& landmark_id_with_index :
       landmark_ids_to_indices_) {
    // Substract 1 as variables_ptr is pointing to lprec struct data array
    // which is indexed from 0 (contrary to row indexing in the problem).
    if (fabs(variables_ptr[landmark_id_with_index.second - 1] - 1.0) <
        std::numeric_limits<float>::epsilon()) {
      summary_landmark_ids->emplace(landmark_id_with_index.first);
      ++num_landmarks_left;
    }
  }
  CHECK_EQ(desired_num_landmarks, num_landmarks_left);

  delete_lp(lprec_ptr.lprec_ptr);
}

void LpSolveDHSparsification::addKeyframeConstraint(
    unsigned int min_keypoints_per_keyframe,
    const vi_map::LandmarkIdSet& keyframe_landmarks,
    const LprecWrapper& lprec_ptr) const {
  CHECK_NOTNULL(lprec_ptr.lprec_ptr);

  // Add 1 as lp_solve is skipping value at index 0.
  const unsigned int kRowSize = 1 + num_variables_;
  // Initialize the row with kRowSize zeros.
  std::vector<REAL> row(kRowSize, 0.0);

  unsigned int landmarks_in_the_segment = 0;
  for (const vi_map::LandmarkId& landmark_id : keyframe_landmarks) {
    StoreLandmarkIdToIndexMap::const_iterator it =
        landmark_ids_to_indices_.find(landmark_id);
    if (it != landmark_ids_to_indices_.end()) {
      // This landmark is present in the segment being summarized.
      row[it->second] = 1.0;
      ++landmarks_in_the_segment;
    }
  }

  // Slack variable with weight 1.0.
  row[num_variables_] = 1.0;

  // Row mode editing is useful (and recommended) to make the constraint
  // addition faster.
  set_add_rowmode(lprec_ptr.lprec_ptr, TRUE);
  add_constraint(
      lprec_ptr.lprec_ptr, &row.front(), GE,
      std::min(min_keypoints_per_keyframe, landmarks_in_the_segment));
  // Return from row mode to standard mode.
  set_add_rowmode(lprec_ptr.lprec_ptr, FALSE);
}

void LpSolveDHSparsification::addTotalLandmarksConstraint(
    unsigned int desired_num_landmarks, const LprecWrapper& lprec_ptr) const {
  CHECK_NOTNULL(lprec_ptr.lprec_ptr);

  // Add 1 as lp_solve is skipping value at index 0.
  const unsigned int kRowSize = 1 + num_variables_;
  // Each landmark has cost 0.
  std::vector<REAL> row(kRowSize, 1.0);

  // Slack variable is not contributing to this constraint.
  row[num_variables_] = 0.0;

  set_add_rowmode(lprec_ptr.lprec_ptr, TRUE);
  add_constraint(lprec_ptr.lprec_ptr, &row.front(), LE, desired_num_landmarks);
  set_add_rowmode(lprec_ptr.lprec_ptr, FALSE);
}

void LpSolveDHSparsification::addObjectiveFunction(
    const vi_map::VIMap& map, const LprecWrapper& lprec_ptr) const {
  CHECK_NOTNULL(lprec_ptr.lprec_ptr);

  const unsigned int kRowSize = 1 + num_variables_;
  std::vector<REAL> row(kRowSize, 0.0);

  const int64_t kSlackVariableCost = 1e10;



  StoreLandmarkIdTodistancecostMap landmark_ids_to_distance_costs_;



  for (const StoreLandmarkIdToIndexMap::value_type& landmark_id_with_index :
       landmark_ids_to_indices_) {
    const vi_map::LandmarkId& landmark_id = landmark_id_with_index.first;

    REAL score = static_cast<REAL>(
        map.getLandmark(landmark_id).getObservations().size());
    row[landmark_id_with_index.second] = score;
  }

  // Slack variable with penalty weight. We maximize our objective so
  // slack variable introduces negative score.
  row[num_variables_] = -kSlackVariableCost;

  set_obj_fn(lprec_ptr.lprec_ptr, &row.front());
  set_maxim(lprec_ptr.lprec_ptr);
}




void LpSolveDHSparsification::addDHObjectiveFunction(
    const vi_map::VIMap& map, const LprecWrapper& lprec_ptr, \
     const vi_map::LandmarkIdList& all_landmark_ids, const std::vector<pose_graph::VertexIdList>& segment_vertex_id_lists) const {
  CHECK_NOTNULL(lprec_ptr.lprec_ptr);



  const unsigned int kRowSize = 1 + num_variables_;
  std::vector<REAL> row(kRowSize, 0.0);

  const int64_t kSlackVariableCost = 1e10;




  StoreLandmarkIdTodistancecostMap landmark_ids_to_distance_costs_;
  setDistancecosts(map, all_landmark_ids, segment_vertex_id_lists, landmark_ids_to_distance_costs_);

  //debuggin : mean and standard deviation
  double distance_cost_mean = 0;
  double distance_cost_std  = 0;
  double observation_count_mean = 0;
  double observation_count_std  = 0;

  // find max observation count to normalize observation count score
  double max_observation = -1.;
  for (const StoreLandmarkIdToIndexMap::value_type& landmark_id_with_index :
       landmark_ids_to_indices_) {
    const vi_map::LandmarkId& landmark_id = landmark_id_with_index.first;
    
    if (static_cast<double>(map.getLandmark(landmark_id).getObservations().size()) > max_observation){
      max_observation = static_cast<double>(map.getLandmark(landmark_id).getObservations().size());
    }
  }

  //check error
  if (max_observation == -1.){
    std::cout << "Max observation error in addDHObjectiveFunction" << std::endl;
  }

  // assign score
  for (const StoreLandmarkIdToIndexMap::value_type& landmark_id_with_index :
       landmark_ids_to_indices_) {
    const vi_map::LandmarkId& landmark_id = landmark_id_with_index.first;

    // REAL score = static_cast<REAL>(
    //     map.getLandmark(landmark_id).getObservations().size());
    // distance histogram cost with observation 
    REAL score = static_cast<REAL>(
        // weighted sum of distance score and observation score
        landmark_ids_to_distance_costs_[landmark_id]*0.9+\
         static_cast<double>(map.getLandmark(landmark_id).getObservations().size())/max_observation*0.1 );
        // distance score only
        //landmark_ids_to_distance_costs_[landmark_id]);
    row[landmark_id_with_index.second] = score;
  
    distance_cost_mean += landmark_ids_to_distance_costs_[landmark_id];
    observation_count_mean += static_cast<double>(map.getLandmark(landmark_id).getObservations().size());

    // check distance cost and observation cost
    //std::cout.precision(8);
    //std::cout << "[distance_cost: " << landmark_ids_to_distance_costs_[landmark_id] \
    << "      observation_count: " << static_cast<double>(map.getLandmark(landmark_id).getObservations().size()) << "]" << std::endl;

    // check unconsidered landmarks. Unconsidered landmarks in calculating distance cost have 0 cost. 
    /*
    // if distance cost < some minimum, print cost and check vertices observing the landmark are on the segment vetcies list
    if (landmark_ids_to_distance_costs_[landmark_id] < 0.00000001){
      // get observing vertices
      pose_graph::VertexIdSet observer_ids_set;
      map.getObserverVerticesForLandmark(landmark_id, &observer_ids_set);
      
      for (const pose_graph::VertexId& observer_id : observer_ids_set){
        for(pose_graph::VertexIdList connected_vertex_list : segment_vertex_id_lists ){
          auto it = find(connected_vertex_list.begin(),connected_vertex_list.end(),observer_id);
          if(it != connected_vertex_list.end()){
            std::cout << "error!! it a vertex in on list but, a landmarks[ " << landmark_id << "] is not considered with cost: " << landmark_ids_to_distance_costs_[landmark_id] << std::endl;
          }
        }
      }
    } // if distance cost < some minimum, print cost and check vertices observing the landmark are on the segment vetcies list
    */
  }
  
  distance_cost_mean = distance_cost_mean / landmark_ids_to_indices_.size();
  observation_count_mean = observation_count_mean / landmark_ids_to_indices_.size();

  std::cout << "[distance_cost_mean : " << distance_cost_mean << " normalized observation cost mean: "<< observation_count_mean/max_observation <<  " observation_count_mean: " << observation_count_mean << "]" << std::endl;

  // Slack variable with penalty weight. We maximize our objective so
  // slack variable introduces negative score.
  row[num_variables_] = -kSlackVariableCost;

  set_obj_fn(lprec_ptr.lprec_ptr, &row.front());
  set_maxim(lprec_ptr.lprec_ptr);
}


// map, vertices, landmarks
void LpSolveDHSparsification::setDistancecosts(const vi_map::VIMap& map, const vi_map::LandmarkIdList& all_landmark_ids, \
 const std::vector<pose_graph::VertexIdList>& segment_vertex_id_lists, StoreLandmarkIdTodistancecostMap& landmark_ids_to_distance_costs) const {

  const double kchuncklength = 30.;
  const double distance_threshold = 50.0;
  const double distance_max_threshold = 500.;
  // linear value : 82.356 / exponential value 82.356*2.5
  const double k_rota_tran = 82.356;
  // cost to isolated landmarks observed isolated a vertex.
  const double cost_to_isolated_landmarks_with_a_isolated_vertex = 0.00001;
  // exception cost. It should be lower than every possible cost.
  const double cost_to_classfication_unknown = 0.00000001; 

  vi_map::LandmarkIdList::const_iterator it;

  // Initailize distance costs
  for(vi_map::LandmarkId landmark_id  : all_landmark_ids){
    landmark_ids_to_distance_costs.emplace(landmark_id, 0.);
  }

  
  unsigned int start_idx;
  unsigned int last_idx;
  double translation;
  double rotation;
  double length_of_all_vertices = 0;
  // calculate spanning all vertices in a partitioned map to get length_of_all_vertices
  for(pose_graph::VertexIdList connected_vertex_list : segment_vertex_id_lists ){
    for(unsigned int int_idx = 0; int_idx < connected_vertex_list.size()-1; int_idx++){
      const vi_map::Vertex& current_vertex = map.getVertex(connected_vertex_list[int_idx]);
      const vi_map::Vertex& next_vertex = map.getVertex(connected_vertex_list[int_idx+1]);
      length_of_all_vertices += (next_vertex.get_p_M_I() - current_vertex.get_p_M_I()).norm();
    }
  }


  // span segment_vertex_id_list
  for(pose_graph::VertexIdList connected_vertex_list : segment_vertex_id_lists ){
    start_idx = 0;
    translation = 0;
    rotation = 0;
    
    // normal case
    for(unsigned int int_idx = 0; int_idx < connected_vertex_list.size()-1; int_idx++){
      const vi_map::Vertex& current_vertex = map.getVertex(connected_vertex_list[int_idx]);
      const vi_map::Vertex& next_vertex = map.getVertex(connected_vertex_list[int_idx+1]);

      // accumulate translation and rotation
      translation += (next_vertex.get_p_M_I() - current_vertex.get_p_M_I()).norm();
      rotation += std::abs(next_vertex.get_q_M_I().angularDistance(current_vertex.get_q_M_I()));

      // divide on kchuncklength and exceptional condition when it get to end of list.
      if ( (translation >= kchuncklength) || (int_idx == connected_vertex_list.size()-2)  ){
        last_idx = int_idx+1; // index of next vertex

        // gather landmarks set from start_idx to last_idx and make distance histogram
        typedef std::pair<vi_map::LandmarkId, Landmark_DH_params> l_dh_vaule_type;
        std::unordered_map<vi_map::LandmarkId, Landmark_DH_params > segment_landmark_id_subset;

        for(unsigned int inner_idx = start_idx; inner_idx <= last_idx; inner_idx ++){
          vi_map::LandmarkIdList landmark_ids;
          const vi_map::Vertex& a_vertex = map.getVertex(connected_vertex_list[inner_idx]);
          a_vertex.getAllObservedLandmarkIds(&landmark_ids);
          // span a landamrk observed by a vertex in a map
          for(vi_map::LandmarkId a_landmark_id : landmark_ids ){ 
            
            it = find(all_landmark_ids.begin(),all_landmark_ids.end(),a_landmark_id);
            // span a landamrk observed by a vertex in a partitioned subset
            if( it != all_landmark_ids.end() ){ 

              // make distance histogram
              auto inner_it = segment_landmark_id_subset.find(a_landmark_id);
              if(inner_it == segment_landmark_id_subset.end()){
                // assign new distance histogram
                segment_landmark_id_subset[a_landmark_id] = Landmark_DH_params();
              }

              // calculate distance between a vetex and a landmark
              Eigen::Vector3d I_p_fi = map.getLandmark_p_I_fi(a_landmark_id, a_vertex); 
              double distance = I_p_fi.norm();
              if(distance < distance_threshold){
                //close count up
                segment_landmark_id_subset[a_landmark_id].close_countup();
              }else if(distance < distance_max_threshold){
                // far count up
                segment_landmark_id_subset[a_landmark_id].far_countup();
              } // calculate distance between a vetex and a landmark


            } // span a landamrk observed by a vertex in a partitioned subset
          } // span a landamrk observed by a vertex in a map
        } // gather landmarks set from start_idx to last_idx and make distance histogram


        // calculate score ratio on near and far class
        const double remaining_far_ratio = \
         // exponenetial ratio without bias
         //exp(k_rota_tran*rotation) / ( exp(k_rota_tran*rotation) + exp(translation) ); 
         // linear ratio without bias
         k_rota_tran*rotation / ( k_rota_tran*rotation + translation );
         // linear ratio with bias
         // (k_rota_tran*rotation / ( k_rota_tran*rotation + translation ))*0.5 + 0.5;


        const double remaining_close_ratio = \
        // without bias
         std::max( 1 - remaining_far_ratio , 0. );
        // with bias 0.5
        //std::max( 1 - remaining_far_ratio , 0.5 );
        const double a_chunk_ratio = translation / length_of_all_vertices;

        // accumulate landmark score depending on.
        // landmark_ids_to_distance_costs[landmark_id] += cost/close or far
        for(const l_dh_vaule_type& a_landmark_with_DH : segment_landmark_id_subset){
          vi_map::LandmarkId landmark_id = a_landmark_with_DH.first;
          Landmark_DH_params landmark_param = a_landmark_with_DH.second;
          
          if(landmark_param.get_classification() == LpSolveDHSparsification::CLASSIFIED::CLOSE){
            landmark_ids_to_distance_costs[landmark_id] += remaining_close_ratio * a_chunk_ratio;
          }else if(landmark_param.get_classification() == LpSolveDHSparsification::CLASSIFIED::FAR){
            landmark_ids_to_distance_costs[landmark_id] += remaining_far_ratio * a_chunk_ratio;
          // unknown with 0 value exception
          }else if( (landmark_param.get_classification() == LpSolveDHSparsification::CLASSIFIED::UNKNOWN) \
           & (landmark_ids_to_distance_costs[landmark_id] < std::numeric_limits<double>::epsilon() ) ){
            landmark_ids_to_distance_costs[landmark_id] = cost_to_classfication_unknown;
          }

          /*
          // for checking error
          if (landmark_ids_to_distance_costs[landmark_id] > 80.){
            if(landmark_param.get_classification() == LpSolveDHSparsification::CLASSIFIED::CLOSE){
              std::cout << "distance cost error!! [landmark id: " << landmark_id << "  cost: " << landmark_ids_to_distance_costs[landmark_id] << \
            " classification: " << "CLOSE" << " remaining_close_ratio: " << remaining_close_ratio << "a_chunk_ratio: " << a_chunk_ratio << \
            " translation: " << translation << " all_length: " << length_of_all_vertices << "]" << std::endl;
            }else if(landmark_param.get_classification() == LpSolveDHSparsification::CLASSIFIED::FAR){
            std::cout << "distance cost error!! [landmark id: " << landmark_id << "  cost: " << landmark_ids_to_distance_costs[landmark_id] << \
            " classification: " << "FAR" << " remaining_close_ratio: " << remaining_close_ratio << "a_chunk_ratio: " << a_chunk_ratio << "]" << std::endl;
            }

          }
          */
        }
        /*
        // for checking error
        if (a_chunk_ratio > 1){
          std::cout << "chunk ratio error!! [translation: " << translation << ", length_of_all_vertices: " << length_of_all_vertices << "]" << std::endl;
        }
        if (remaining_far_ratio > 1){
          std::cout << "remaining_far_ratio error!! [remaining_far_ratio: " << remaining_far_ratio << "]" << std::endl;
        }
        */

        //clear all terms
        translation = 0;
        rotation = 0;
        start_idx = last_idx;


      }// if (translation >= kchuncklength)

    } // normal case

    // exceptional case: a chunk consists of only a vertices.
    // To satisfy minimum k constraints, add minimum cost to the landmarks belongs to this vertex.
    if(connected_vertex_list.size() == 1){
      vi_map::LandmarkIdList landmark_ids;
      const vi_map::Vertex& a_vertex = map.getVertex(connected_vertex_list[0]);
      a_vertex.getAllObservedLandmarkIds(&landmark_ids);

      // span a landamrk observed by a vertex in a map
      for(vi_map::LandmarkId a_landmark_id : landmark_ids ){ 
        it = find(all_landmark_ids.begin(),all_landmark_ids.end(),a_landmark_id);
        // span a landamrk observed by a vertex in a partitioned subset
        if( ( it != all_landmark_ids.end() ) & (landmark_ids_to_distance_costs[a_landmark_id] < std::numeric_limits<double>::epsilon() ) ){ 
          // insert minimum distance cost to a only isolated landmarks observed a isolated vertex. not landmarks observed other vertices (distanec cost is not 0 case).
          landmark_ids_to_distance_costs[a_landmark_id] = cost_to_isolated_landmarks_with_a_isolated_vertex;
        }
      }
    }// exceptional case 

  } // span segment_vertex_id_list
} // void LpSolveDHSparsification::setDistancecosts
  










void LpSolveDHSparsification::setLandmarkSwitchVariablesToBinary(
    const LprecWrapper& lprec_ptr) const {
  for (unsigned int i = 1; i <= landmark_ids_to_indices_.size(); ++i) {
    set_binary(lprec_ptr.lprec_ptr, i, TRUE);
  }
  // Slack variable is an integer one.
  set_int(lprec_ptr.lprec_ptr, num_variables_, TRUE);
}

}  // namespace map_sparsification
